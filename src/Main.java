import services.*;
import services.Redis.ListService;
import services.Redis.SetService;
import services.Redis.StringService;

import java.io.IOException;
import java.util.*;

public class Main {

    public static String[] stringArray = new String[]{"set", "get", "del", "append"};
    public static List stringCommands = new ArrayList<>(Arrays.asList(stringArray));

    public static String[] setArray = new String[]{"hset", "hget", "hdel", "hgetall", "hlen"};
    public static List setCommands = new ArrayList<>(Arrays.asList(setArray));

    public static String[] listArray = new String[]{"lpush", "rpush", "lrange", "lrem"};
    public static List listCommands = new ArrayList<>(Arrays.asList(listArray));

    public static void main(String[] args) throws IOException {

        while (true) {
            System.out.println("Enter valid command");

            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();

            if (command.equals("q")) {
                break;
            }


            String commandType = getFirstWordOfString(command).toLowerCase();

            if (stringCommands.contains(commandType)) {
                StringService service = new StringService();

                boolean isValidCommand = service.isValidCommand(command);

                if (!isValidCommand) {
                    System.out.println("Not valid command");
                    continue;
                }

                switch (commandType) {
                    case "set":
                        service.set(command);
                        break;
                    case "get":
                        service.get(command);
                        break;
                    case "del":
                        service.del(command);
                        break;
                    case "append":
                        service.append(command);
                        break;

                }
            } else if (setCommands.contains(commandType)) {
                SetService service = new SetService();


                boolean isValidCommand = service.isValidCommand(command);

                if (!isValidCommand) {
                    System.out.println("Not valid command");
                    continue;
                }

                switch (commandType) {
                    case "hset":
                        service.set(command);
                        break;
                    case "hget":
                        service.get(command);
                        break;
                    case "hdel":
                        service.del(command);
                        break;
                    case "hgetall":
                        service.getall(command);
                        break;
                    case "hlen":
                        service.len(command);
                        break;

                }
            } else if (listCommands.contains(commandType)) {
                ListService service = new ListService();

                System.out.println(command);
                boolean isValidCommand = service.isValidCommand(command);

                if (!isValidCommand) {
                    System.out.println("Not valid command");
                    continue;
                }

                switch (commandType) {
                    case "lpush":
                        service.lpush(command);
                        break;
                    case "rpush":
                        service.rpush(command);
                        break;
                    case "lrange":
                        service.lrange(command);
                        break;
                    case "lrem":
                        service.lrem(command);
                        break;
                }
            }
        }
    }

    public static String getFirstWordOfString(String string) {
        String arr[] = string.split(" ", 2);

        return arr[0];
    }
}
