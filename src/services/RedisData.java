package services;

import java.util.HashMap;

public enum RedisData {
    INSTANCE;

    HashMap value = new HashMap<String, Object>();

    public HashMap getValue() {
        return value;
    }

    public void setValue(HashMap value) {
        this.value = value;
    }
}