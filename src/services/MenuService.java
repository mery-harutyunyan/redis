package services;

import services.Redis.ListService;
import services.Redis.SetService;
import services.Redis.StringService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MenuService {
    public static List stringCommands = new ArrayList<>(Arrays.asList(new String[]{"set", "get", "del", "append"}));
    public static List setCommands = new ArrayList<>(Arrays.asList(new String[]{"hset", "hget", "hdel", "hgetall", "hlen"}));
    public static List listCommands = new ArrayList<>(Arrays.asList(new String[]{"lpush", "rpush", "lrange", "lrem"}));

    public static String start(String command) {

        String commandType = getFirstWordOfString(command).toLowerCase();

        String response = "Done";
        if (stringCommands.contains(commandType)) {
            StringService service = new StringService();

            boolean isValidCommand = service.isValidCommand(command);

            if (!isValidCommand) {
                response = "Not valid command";
            }
            switch (commandType) {
                case "set":
                    response = service.set(command);
                    break;
                case "get":
                    response = service.get(command);
                    break;
                case "del":
                    response = service.del(command);
                    break;
                case "append":
                    response = service.append(command);
                    break;

            }
        } else if (setCommands.contains(commandType)) {
            SetService service = new SetService();

            boolean isValidCommand = service.isValidCommand(command);

            if (!isValidCommand) {
                response = "Not valid command";
            }

            switch (commandType) {
                case "hset":
                    response = service.set(command);
                    break;
                case "hget":
                    response = service.get(command);
                    break;
                case "hdel":
                    response = service.del(command);
                    break;
                case "hgetall":
                    response = service.getall(command);
                    break;
                case "hlen":
                    response = String.valueOf(service.len(command));
                    break;

            }
        } else if (listCommands.contains(commandType)) {
            ListService service = new ListService();

            boolean isValidCommand = service.isValidCommand(command);

            if (!isValidCommand) {
                response = "Not valid command";
            }

            switch (commandType) {
                case "lpush":
                    response = service.lpush(command);
                    break;
                case "rpush":
                    response = service.rpush(command);
                    break;
                case "lrange":
                    response = service.lrange(command);
                    break;
                case "lrem":
                    response = service.lrem(command);
                    break;
            }
        }
        return response;
    }

    public static String getFirstWordOfString(String string) {
        String arr[] = string.split(" ", 2);

        return arr[0];
    }
}
