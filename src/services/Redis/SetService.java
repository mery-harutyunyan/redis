package services.Redis;

import services.RedisData;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SetService {

    public static final String HSET = "hset\\s+(\\w+)\\s+(\\w+)\\s+(\\w+)";
    public static final String HGET = "hget\\s+(\\w+)\\s+(\\w+)";
    public static final String HDEL = "hdel\\s+(\\w+)\\s+(\\w+)";
    public static final String HGETALL = "hgetall\\s+(\\w+)";
    public static final String HLEN = "hlen\\s+(\\w+)";

    public boolean isValidCommand(String command) {
        return command.matches(HSET)
                || command.matches(HGET)
                || command.matches(HDEL)
                || command.matches(HGETALL)
                || command.matches(HLEN);
    }

    public String set(String command) {
        Pattern re = Pattern.compile(HSET, Pattern.CASE_INSENSITIVE);
        Matcher m = re.matcher(command);

        String field = null;
        String key = null;
        String value = null;
        while (m.find()) {
            field = m.group(1);
            key = m.group(2);
            value = m.group(3);
        }

        HashMap data = RedisData.INSTANCE.getValue();
        HashMap newData = new HashMap();
        if (data.containsKey(field))
            newData = (HashMap) data.get(field);
        newData.put(key, value);
        data.put(field, newData);
        RedisData.INSTANCE.setValue(data);
        return "OK";
    }

    public String get(String command) {
        Pattern re = Pattern.compile(HGET, Pattern.CASE_INSENSITIVE);
        Matcher m = re.matcher(command);

        String field = null;
        String key = null;
        while (m.find()) {
            field = m.group(1);
            key = m.group(2);
        }

        HashMap data = RedisData.INSTANCE.getValue();
        HashMap newData = (HashMap) data.get(field);
        return newData.get(key).toString();
    }

    public String del(String command) {
        Pattern re = Pattern.compile(HDEL, Pattern.CASE_INSENSITIVE);
        Matcher m = re.matcher(command);

        String field = null;
        String key = null;
        while (m.find()) {
            field = m.group(1);
            key = m.group(2);
        }

        HashMap data = RedisData.INSTANCE.getValue();
        HashMap newData = (HashMap) data.get(field);
        newData.remove(key);
        data.put(field, newData);
        RedisData.INSTANCE.setValue(data);
        return "deleted";
    }

    public String getall(String command) {
        Pattern re = Pattern.compile(HGETALL, Pattern.CASE_INSENSITIVE);
        Matcher m = re.matcher(command);

        String field = null;
        while (m.find()) {
            field = m.group(1);
        }

        HashMap data = RedisData.INSTANCE.getValue();
        HashMap newData = (HashMap) data.get(field);
        return newData.toString();
    }

    public int len(String command) {
        Pattern re = Pattern.compile(HLEN, Pattern.CASE_INSENSITIVE);
        Matcher m = re.matcher(command);

        String field = null;
        while (m.find()) {
            field = m.group(1);
        }

        HashMap data = RedisData.INSTANCE.getValue();
        HashMap newData = (HashMap) data.get(field);

        return newData.size();
    }

}