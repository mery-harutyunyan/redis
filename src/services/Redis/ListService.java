package services.Redis;

import services.RedisData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ListService {

    public static final String LPUSH = "lpush\\s+(\\w+)\\s+(\\w+)";
    public static final String RPUSH = "rpush\\s+(\\w+)\\s+(\\w+)";
    public static final String LRANGE = "lrange\\s+(\\w+)\\s+(-?\\d+)\\s+(-?\\d+)";
    public static final String LREM = "lrem\\s+(\\w+)\\s+(\\w+)";

    public boolean isValidCommand(String command) {
        return command.matches(LPUSH)
                || command.matches(RPUSH)
                || command.matches(LRANGE)
                || command.matches(LREM);
    }

    public String lpush(String command) {
        Pattern re = Pattern.compile(LPUSH, Pattern.CASE_INSENSITIVE);
        Matcher m = re.matcher(command);

        String field = null;
        String value = null;
        while (m.find()) {
            field = m.group(1);
            value = m.group(2);
        }

        HashMap data = RedisData.INSTANCE.getValue();
        List<String> newData = new ArrayList<>();
        if (data.containsKey(field)) {
            newData = (List) data.get(field);
        }
        newData.add(0, value);
        data.put(field, newData);
        RedisData.INSTANCE.setValue(data);
        return "OK";
    }

    public String rpush(String command) {
        Pattern re = Pattern.compile(RPUSH, Pattern.CASE_INSENSITIVE);
        Matcher m = re.matcher(command);

        String field = null;
        String value = null;
        while (m.find()) {
            field = m.group(1);
            value = m.group(2);
        }

        HashMap data = RedisData.INSTANCE.getValue();
        List<String> newData = new ArrayList<>();
        if (data.containsKey(field)) {
            newData = (List) data.get(field);
        }
        newData.add(value);
        data.put(field, newData);
        RedisData.INSTANCE.setValue(data);
        return "OK";
    }

    public String lrange(String command) {
        Pattern re = Pattern.compile(LRANGE, Pattern.CASE_INSENSITIVE);
        Matcher m = re.matcher(command);

        String field = null;
        int startIndex = 0;
        int endIndex = 0;
        while (m.find()) {
            field = m.group(1);
            startIndex = Integer.parseInt(m.group(2));
            endIndex = Integer.parseInt(m.group(3));
        }

        HashMap data = RedisData.INSTANCE.getValue();
        List<String> newData = new ArrayList<>();
        if (data.containsKey(field)) {
            newData = (List) data.get(field);
        }
        return newData.subList(startIndex, endIndex).toString();
    }

    public String lrem(String command) {
        Pattern re = Pattern.compile(LREM, Pattern.CASE_INSENSITIVE);
        Matcher m = re.matcher(command);

        String field = null;
        String value = null;
        while (m.find()) {
            field = m.group(1);
            value = m.group(2);
        }

        HashMap data = RedisData.INSTANCE.getValue();
        List<String> newData = new ArrayList<>();
        if (data.containsKey(field)) {
            newData = (List) data.get(field);
        }

        String finalValue = value;
        newData.removeIf(name -> name.equals(finalValue));
        data.put(field, newData);
        RedisData.INSTANCE.setValue(data);
        return "Deleted";
    }

}
