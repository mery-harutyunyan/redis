package services.Redis;

import services.RedisData;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringService {

    public static final String SET = "set\\s+(\\w+)\\s+(\\w+)";
    public static final String APPEND = "append\\s+(\\w+)\\s+(\\w+)";
    public static final String GET = "get\\s+(\\w+)";
    public static final String DEL = "del\\s+(\\w+)";

    public boolean isValidCommand(String command) {
        return command.matches(SET)
                || command.matches(GET)
                || command.matches(DEL)
                || command.matches(APPEND);
    }

    public String set(String command) {
        Pattern re = Pattern.compile(SET, Pattern.CASE_INSENSITIVE);
        Matcher m = re.matcher(command);

        String field = null;
        String value = null;
        while (m.find()) {
            field = m.group(1);
            value = m.group(2);
        }

        HashMap data = RedisData.INSTANCE.getValue();
        data.put(field, value);
        RedisData.INSTANCE.setValue(data);
        return "OK";
    }

    public String get(String command) {
        Pattern re = Pattern.compile(GET, Pattern.CASE_INSENSITIVE);
        Matcher m = re.matcher(command);

        String field = null;
        while (m.find()) {
            field = m.group(1);
        }

        HashMap data = RedisData.INSTANCE.getValue();
        return data.get(field).toString();
    }

    public String del(String command) {
        Pattern re = Pattern.compile(DEL, Pattern.CASE_INSENSITIVE);
        Matcher m = re.matcher(command);

        String field = null;
        while (m.find()) {
            field = m.group(1);
        }

        HashMap data = RedisData.INSTANCE.getValue();
        data.remove(field);
        RedisData.INSTANCE.setValue(data);
        return data.toString();
    }

    public String append(String command) {
        Pattern re = Pattern.compile(APPEND, Pattern.CASE_INSENSITIVE);
        Matcher m = re.matcher(command);

        String field = null;
        String value = null;
        while (m.find()) {
            field = m.group(1);
            value = m.group(2);
        }

        HashMap data = RedisData.INSTANCE.getValue();
        data.put(field, data.get(field) + value);
        RedisData.INSTANCE.setValue(data);

        return "ok";
    }

}